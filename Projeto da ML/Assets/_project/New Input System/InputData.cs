﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct InputData
{

    private bool _jump;
    private bool _menuButton;
    private bool _shoot;
    private bool _run;
    private Vector2 _movement;
    private Vector2 _mouseMovemant;

    public bool Run
    {
        get { return _run; }
        set { _run = value; }
    }
    public bool Jump
    {
        get { return _jump; }
        set { _jump = value; }
    }
    public Vector2 Movement
    {
        get { return _movement; }
        set { _movement = value; }
    }
    public bool MenuButton
    {
        get { return _menuButton; }
        set { _menuButton = value; }
    }
    public bool Shoot
    {
        get { return _shoot; }
        set { _shoot = value; }
    }
    public Vector2 MouseMovement
    {
        get { return _mouseMovemant; }
        set { _mouseMovemant = value; }
    }
}
