﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputListner : MonoBehaviour
{
    [Header("Controller")]
    [SerializeField] private InputData _playerInputData = default;
    private InputMaster _controller;

    public delegate void PlayerInputs(InputData inputData);
    public PlayerInputs OnPlayerInput;

    public void InitializeControllers()
    {

        _controller = new InputMaster();
        _controller.Enable();

        _controller.Player.Pause.performed += MenuButton;

        _controller.Player.Jump.performed += Jump;
        _controller.Player.MouseMoviment.performed += MouseMovement;

        _controller.Player.Run.performed += StartRun;
        _controller.Player.Run.canceled += EndRun;

        _controller.Player.Shoot.performed += ShootStart;
        _controller.Player.Shoot.canceled += ShootEnd;

        _controller.Player.Moviment.performed += Moviment;
        _controller.Player.Moviment.canceled += Moviment;

    }


    private void Update()
    {
        OnPlayerInput?.Invoke(_playerInputData);
        ResetInputs();
    }

    private void Jump(InputAction.CallbackContext context)
    {
        _playerInputData.Jump = true;
    }
    private void Moviment(InputAction.CallbackContext context)
    {
        _playerInputData.Movement = context.ReadValue<Vector2>();
    }
    private void MenuButton(InputAction.CallbackContext context)
    {
        _playerInputData.MenuButton = true;
    }
    private void ShootStart(InputAction.CallbackContext context)
    {
        _playerInputData.Shoot = true;
    }
    private void ShootEnd(InputAction.CallbackContext context)
    {
        _playerInputData.Shoot = false;
    }
    private void MouseMovement(InputAction.CallbackContext context)
    {
        _playerInputData.MouseMovement = context.ReadValue<Vector2>();
    }
    private void StartRun(InputAction.CallbackContext context)
    {
        _playerInputData.Run = true;
    }
    private void EndRun(InputAction.CallbackContext context)
    {
        _playerInputData.Run = false;
    }

    private void ResetInputs()
    {
        _playerInputData.Jump = false;
        _playerInputData.MenuButton = false;
        //_playerInputData.Shoot = false;
        _playerInputData.MouseMovement = Vector2.zero;
    }



}
