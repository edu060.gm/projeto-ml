﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    [SerializeField] private DropInfo.DropType _dropType = default;
    [SerializeField] private float _timeToDisable = default;

    private ObjectPooler _objectPooler;
    private AudioManager _audioManager;

    protected virtual void Effect(PlayerController player)
    {
    }

    private IEnumerator DisableTimer()
    {
        yield return new WaitForSeconds(_timeToDisable);
        gameObject.SetActive(false);
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        _objectPooler.ReturnGameObject(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out PlayerController player))
        {
            Effect(player);
        }
    }

    private void OnEnable()
    {
        StartCoroutine(DisableTimer());
    }

}
