﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDrop : DropItem
{
    [SerializeField] private float _healthAmount = default;

    protected override void Effect(PlayerController player)
    {
        if(!player.HealthControl.CheckFullLife())
        {
            player.HealthControl.AddHealth(_healthAmount);
            gameObject.SetActive(false);
        }
    }
}
