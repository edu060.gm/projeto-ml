﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoDrop : DropItem
{
    [SerializeField] private ProjectileType.Type _type = default;

    protected override void Effect(PlayerController player)
    {
        if (!player.CharacterGun.CheckIfIsFullAmmo())
        {
            player.CharacterGun.AddAmmo();
            player.CharacterGun.Reload();
            player.CharacterGun.HandleAmmo();

            gameObject.SetActive(false);
        }
    }
}
