﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropsManager : MonoBehaviour
{
    [SerializeField] private List<DropInfo> _drops = default;
    private Dictionary<DropInfo.DropType, DropItem> _dropDictionary = new Dictionary<DropInfo.DropType, DropItem>();   

    private ObjectPooler _objectPooler;

    public DropItem GetDrop(DropInfo.DropType dropType)
    {
        return _objectPooler.GetObject(_dropDictionary[dropType].gameObject).GetComponent<DropItem>();
    }

    public void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();

        foreach(DropInfo drop in _drops)
        {
            _dropDictionary.Add(drop.DropName, drop.Prefab);
        }
    }

    public int DropsCount
    {
        get { return _drops.Count; }
    }

}
