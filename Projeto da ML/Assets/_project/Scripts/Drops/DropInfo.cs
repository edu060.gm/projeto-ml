﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropInfo
{
    public enum DropType { HeathDrop, ArrowDrop };
    public DropType DropName;
    public DropItem Prefab;
}
