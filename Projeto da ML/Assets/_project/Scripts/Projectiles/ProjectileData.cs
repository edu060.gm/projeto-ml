﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Projectile", menuName = "ScriptableObject/ProjectileData")]
public class ProjectileData : ScriptableObject
{
    public float BaseDamage;
    public float LifeTime;

    public LayerMask StuckLayers;

    public List<Sound.SongNames> HitAudios;
}
