﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private ProjectileData _projectileData = default;
    [SerializeField] private Rigidbody _rigidbody = default;
    [SerializeField] private Collider _collider = default;

    private float _actualDamage = 1;
    private AudioManager _audioManager;
    protected virtual void Initialize()
    {
        SetActualDamage();
        StartCoroutine(DisableAfterTime(_projectileData.LifeTime));

        _audioManager = PersistemObjects.sInstance.GetAudioManager();
    }

    public virtual void SetActualDamage(float extraDamage = 0)
    {
        _actualDamage = _projectileData.BaseDamage + extraDamage;
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<IDamageable<float>>() != null)
        {
            collision.gameObject.GetComponent<IDamageable<float>>().GetHit(_actualDamage);
            transform.parent = collision.transform;
            _rigidbody.isKinematic = true;
            _collider.enabled = false;

            PlayRandomAudio(_projectileData.HitAudios);
        }
    }


    protected void PlayRandomAudio(List<Sound.SongNames> listOfSongs)
    {
        int chosenIndex = Random.Range(0, listOfSongs.Count);

        AudioSource audio = _audioManager.GetAudioSource(listOfSongs[chosenIndex]);
        audio.gameObject.transform.position = gameObject.transform.position;
    }

    protected virtual IEnumerator DisableAfterTime(float time)
    {
        StopCoroutine(DisableAfterTime(_projectileData.LifeTime));
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    private void Start()
    {
        Initialize();
    }

    public float ActualDamage
    {
        get { return _actualDamage; }
        set { _actualDamage = value; }
    }
    protected Collider ObjCollider
    {
        get { return _collider; }
        set { _collider = value; }
    }
    protected Rigidbody ObjRigidbody
    { 
        get { return _rigidbody; }
        set { _rigidbody = value; }
    }
    protected ProjectileData ProjectileD
    {
        get { return _projectileData; }
        set { _projectileData = value; }
    }

}
