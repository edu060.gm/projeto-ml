﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : Projectile
{

    private bool _isStuck = false;
    private Quaternion _lastRotation;

    protected override void OnCollisionEnter(Collision collision)
    {
        Stuck(collision);
        DisableAfterTime(ProjectileD.LifeTime);

        if (collision.gameObject.GetComponent<IDamageable<float>>() != null)
        {
            collision.gameObject.GetComponent<IDamageable<float>>().GetHit(ActualDamage);
            StopAllCoroutines();
            gameObject.SetActive(false);

        }

        PlayRandomAudio(ProjectileD.HitAudios);
    }

    protected virtual void Stuck(Collision collision)
    {
        transform.parent = collision.transform;
        transform.rotation = _lastRotation;

        _isStuck = true;

        ObjRigidbody.isKinematic = true;
        ObjCollider.enabled = false;
    }

    private void Update()
    {
        if(_isStuck == false)
        {
            ArrowRotation();
        }
    }

    private void ArrowRotation()
    {
        transform.rotation = Quaternion.LookRotation(ObjRigidbody.velocity);
        _lastRotation = transform.rotation;
    }

}
