﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager sInstance;

    [SerializeField] private PlayerController _player = default;
    [SerializeField] private PauseHandler _pauseHandler = default;
    [SerializeField] private InputListner _inputListner = default;
    [SerializeField] private DropsManager _dropsManager = default;
    [SerializeField] private EnemyManager _enemyManager = default;
    [SerializeField] private GameUIManager _gameUIManager = default;
    [SerializeField] private GameController _gameController = default;
    [SerializeField] private EnemySpawnManager _enemySpawnManager = default;
    [SerializeField] private DistorceView _distorceView = default;


    private void Awake()
    {
        if (sInstance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            sInstance = this;
        }
    }

    public DistorceView GetDistorceView()
    {
        return _distorceView;
    }

    public EnemySpawnManager GetEnemySpawnManager()
    {
        return _enemySpawnManager;
    }

    public PauseHandler GetPauseHandler()
    {
        return _pauseHandler;
    }

    public GameController GetGameController()
    {
        return _gameController;
    }

    public EnemyManager GetEnemyManager()
    {
        return _enemyManager;
    }

    public InputListner GetInputListner()
    {
        return _inputListner;
    }
    
    public DropsManager GetDropsManager()
    {
        return _dropsManager;
    }

    public PlayerController GetPlayerController()
    {
        return _player;
    }

    public GameUIManager GetGameUIManager()
    {
        return _gameUIManager;
    }

    public void SetPlayer(PlayerController player)
    {
        _player = player;
    }


    private void Initialize()
    {
        _inputListner?.InitializeControllers();
        _gameController?.Initialize();
        _dropsManager?.Initialize();
        _gameUIManager?.Initialize();
        _enemySpawnManager?.Initialize();
        _pauseHandler?.Initialize();
    }

    private void Start()
    {
        Initialize();
    }


}
