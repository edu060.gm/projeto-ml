﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    private int _maxEnemiesInGame = 4;
    private int _enemiesInGame;
    
    private int _enemiesKillCount;
    private int _enimiesKillScore;

    public delegate void EnemyManagerDelegate();
    public EnemyManagerDelegate OnEnemyDie;

    public void NewEnemieInGame()
    {
        _enemiesInGame++;
        ControlEnemiesSpawn();
    }

    public void LessOneEnemieInGame(int enemyScoreValue)
    {
        //_enemiesKillCount++;
        _enemiesInGame--;
        _enimiesKillScore += enemyScoreValue;

        ControlEnemiesSpawn();
        OnEnemyDie?.Invoke();

    }


    private void Start()
    {
        Intialize();
    }

    private void Intialize()
    {
        _enemiesKillCount = 0;
        _enemiesInGame = 0;
    }

    private void ControlEnemiesSpawn()
    {

    }

    public int MaxEnemiesInGame
    {
        get { return _maxEnemiesInGame; }
        set { _maxEnemiesInGame = value; }
    }

    public int KillCount
    {
        get { return _enemiesKillCount; }
        set { _enemiesKillCount = value; }
    }

    public int KillScore
    {
        get { return _enimiesKillScore; }
        set { _enimiesKillScore = value; }
    }

    public int EnemiesInGame
    {
        get { return _enemiesInGame; }
    }



}
