﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedSpawner : Spawner
{

    [Header("Animation")]
    [SerializeField] private Animator _animator = default;
    [SerializeField] private float _animationResteTimer = default;

    protected override IEnumerator SpawnCoroutine()
    {
        _animator.SetTrigger(SpawnAnimationData.Spawn);

        yield return new WaitForSeconds(_animationResteTimer);

        _animator.SetTrigger(SpawnAnimationData.Reset);

        
    }

}
