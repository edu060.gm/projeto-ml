﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropsSpawner : MonoBehaviour
{
    [SerializeField] private List<Transform> _spawnTransforms = default;
    [SerializeField] private List<DropInfo.DropType> _drops = default;
    [SerializeField] private float _spawnBreak = default;

    private DropsManager _dropManager;

    public void Initialize()
    {
        _dropManager = GameManager.sInstance.GetDropsManager();

        StartCoroutine(SpawnCoroutive());
    }

    private IEnumerator SpawnCoroutive()
    {
        yield return new WaitForSeconds(_spawnBreak);

        SpawnDrop();

        StartCoroutine(SpawnCoroutive());
    }

    private void SpawnDrop()
    {
        int dropIndex = Random.Range(0, _drops.Count);
        int spawnIndex = Random.Range(0, _spawnTransforms.Count);

        DropItem drop = _dropManager.GetDrop(_drops[dropIndex]);
        drop.transform.position = _spawnTransforms[spawnIndex].position;
    }
}
