﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Transform _spawnPosition = default;
    [SerializeField] private GameObject _prefab = default;

    private ObjectPooler _objectPooler;

    public virtual void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();

        if (_spawnPosition == null)
        {
            _spawnPosition = transform;
        }
    }

    public void StartSpawn()
    {
        StartCoroutine(SpawnCoroutine());
    }

    protected virtual void Spawn()
    {
        GameObject spawned = _objectPooler.GetObject(_prefab);
        spawned.transform.position = _spawnPosition.position;
    }

    protected virtual IEnumerator SpawnCoroutine()
    {
        yield return null;
        Spawn();
    }


    protected Transform SpawnPosition
    {
        get { return _spawnPosition; }
    }

    public GameObject Prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }

}
