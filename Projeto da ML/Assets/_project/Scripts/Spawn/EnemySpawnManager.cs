﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    [SerializeField] private List<Spawner> _spawners = default;
    [SerializeField] private List<BaseEnemy> _enemies = default;

    private float _spawnBreak = 2;
    private bool _canSpawn = true;

    private GameController _gameController;
    private EnemyManager _enemyManager;
    private EnemySpawnManager _enemySpawnManager;

    public void Initialize()
    {
        _enemyManager = GameManager.sInstance.GetEnemyManager();
        _gameController = GameManager.sInstance.GetGameController();

        foreach(Spawner spawner in _spawners)
        {
            spawner.Initialize();
        }

    }

    private void Update()
    {
        if(_gameController.GameStarted)
        {
            SpawnHandle();
        }
    }

    private void SpawnNew()
    {
        Spawner chosenSpawner = ChooseSpawner();
        BaseEnemy chosenEnemy = ChooseEnemy();

        chosenSpawner.Prefab = chosenEnemy.gameObject;
        chosenSpawner.StartSpawn();
    }

    private void SpawnHandle()
    {
        if(_canSpawn)
        {
            if(_enemyManager.EnemiesInGame < _enemyManager.MaxEnemiesInGame)
            {
                SpawnNew();

                _canSpawn = false;
                StartCoroutine(CanSpawnCooldown());
            }
        }
    }

    private BaseEnemy ChooseEnemy()
    {
        int enemyIndex = Random.Range(0, _enemies.Count);

        return _enemies[enemyIndex];

    }

    private Spawner ChooseSpawner()
    {
        int spawnerIndex = Random.Range(0, _spawners.Count);

        return _spawners[spawnerIndex];
    }

    private IEnumerator CanSpawnCooldown()
    {
        yield return new WaitForSeconds(_spawnBreak);

        _canSpawn = true;
    }

}
