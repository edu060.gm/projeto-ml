﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistemObjects : MonoBehaviour
{
    public static PersistemObjects sInstance;

    [SerializeField] private ObjectPooler _objectPooler = default;
    [SerializeField] private AudioManager _audioManager = default;

    private void Awake()
    {
        if(sInstance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            sInstance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public AudioManager GetAudioManager()
    {
        return _audioManager;
    }

    public ObjectPooler GetObjectPooler()
    {
        return _objectPooler;
    }


    private void Start()
    {
        Intialize();
    }

    public void Intialize()
    {
        _audioManager.Initialize();
    }


}
