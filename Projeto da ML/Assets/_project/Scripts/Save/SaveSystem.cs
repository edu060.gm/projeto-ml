﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveSystem
{
    public static string SavePath = Application.persistentDataPath + "/SaveFile.json";

    private static SaveData _localData;

    public static void SaveGame()
    {
        string jsonFile = JsonUtility.ToJson(_localData);

        File.WriteAllText(SaveSystem.SavePath, jsonFile);
    }

    public static void LoadGame()
    {
        if (File.Exists(SaveSystem.SavePath))
        {
            string jsonSavedFile = File.ReadAllText(SaveSystem.SavePath);
            SaveData saveFile = JsonUtility.FromJson<SaveData>(jsonSavedFile);

            _localData = saveFile;
        }
        else
        {
            _localData = new SaveData();

            _localData.MasterVolume = 0.5f;
            _localData.MusicsVolume = 0.5f;
            _localData.EffectVolume = 0.5f;

            _localData.GameTime = 1;

            string jsonFile = JsonUtility.ToJson(_localData);
            File.WriteAllText(SaveSystem.SavePath, jsonFile);
        }
    }

    public static SaveData LocalData
    {
        get 
        {
            if (_localData == null)
            {
                LoadGame();
            }

            return _localData; 
        }
    }

}
