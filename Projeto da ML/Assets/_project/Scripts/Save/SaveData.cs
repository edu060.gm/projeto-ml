﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{

    public float MasterVolume;
    public float MusicsVolume;
    public float EffectVolume;

    public int MaxEnemiesDead;

    public int GameTime;
}
