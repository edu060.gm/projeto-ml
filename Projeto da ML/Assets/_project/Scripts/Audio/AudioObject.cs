﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioObject : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    private ObjectPooler _objectPooler;
    private Coroutine _waitForAudioEndCoroutine;

    public void StopAudio()
    {
        
        _objectPooler.ReturnGameObject(this.gameObject);
        gameObject.SetActive(false);

    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
        gameObject.SetActive(true);
    }

    private void PlayAudio()
    {
        _audio.Play();
        if(_waitForAudioEndCoroutine != null)
        {
            //_audio.Stop();
            StopCoroutine(_waitForAudioEndCoroutine);
        }

        if(_audio.loop == false)
        {
            _waitForAudioEndCoroutine = StartCoroutine(WaitAudioEnd(_audio.clip.length / _audio.pitch));
        }
    }

    private void OnEnable()
    {
        PlayAudio();
    }

    private IEnumerator WaitAudioEnd(float audioLength)
    {
        yield return new WaitForSeconds(audioLength);

        StopAudio();
    }

}
