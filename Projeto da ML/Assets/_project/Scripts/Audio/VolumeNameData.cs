﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeNameData
{
    public static string MasterVolume = "MasterVolume";
    public static string EffectVolume = "EffectVolume";
    public static string MusicsVolume = "MusicVolume";
}
