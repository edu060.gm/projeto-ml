﻿using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private Dictionary<Sound.SongNames, Sound> _soundDic = new Dictionary<Sound.SongNames, Sound>();
    [SerializeField] private Sound[] _sounds;

    private ObjectPooler _objectPooler;

    public AudioSource GetAudioSource(Sound.SongNames name)
    {
        return _objectPooler.GetObject(_soundDic[name].AudioObject).GetComponent<AudioSource>();
    }


    public void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();   

        foreach (Sound s in _sounds)
        {
            _soundDic.Add(s.Name, s);
        }
    }

}
