﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{

    public enum SongNames { THEME, COMBAT, OPTIONS, LOSE, WIN, SWORD_HIT_1, SWORD_HIT_2,
    SWORD_HIT_3, SWORD_HIT_4, FOOTSTEP_R, FOOTSTEP_L, THUNDER_1, THUNDER_2, THUNDER_3,
    ARROW_REALESE_1, ARROW_REALESE_2, ARROW_REALESE_3, ARROW_REALESE_4, ARROW_REALESE_5,
    JUMP, BUTTON, SECOND, PUNCH_HIT_1, PUNCH_HIT_2, ARROW_HIT_1, ARROW_HIT_2, MISS_1, 
    MISS_2, ENEMY_DYING_1, ENEMY_DYING_2, ENEMY_DYING_3, ENEMY_DYING_4, ENEMY_GET_HIT_1, 
    ENEMY_GET_HIT_2, PLAYER_GET_HIT_1, PLAYER_GET_HIT_2, ARROW_HIT_3, ARROW_HIT_4,
    }

    private string name;
    public SongNames Name;
    public AudioSource Source;
    public GameObject AudioObject;


}
