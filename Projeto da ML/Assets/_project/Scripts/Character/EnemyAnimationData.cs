﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationData
{
    public static string Attack = "Attack";
    public static string Die = "Die";
    public static string Block = "Block";
    public static string BlockBeHit = "BlockBeHit";
    public static string BeHit = "BeHit";
    public static string Velocity = "Velocity";
    public static string Idle = "Idle";
}
