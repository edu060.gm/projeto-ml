﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseEnemy : Character
{

    [SerializeField] private AnimationEventHandle _animationHandle = default;

    [Header("Moviment")]
    [SerializeField] private NavMeshAgent _navMashAgent = default;
    [SerializeField] private Transform _destination = default;
    private bool _backToLife = false;
    private bool _canWalk = true;

    [Header("Combat")]
    [SerializeField] private Transform _hitCollider;
    private bool _canAttack = true;
    private Character _target;
    private bool _isBlocking;
    private bool _aiming = true;
    private float _frowardOffset;

    [Header("Dying")]
    private bool _goingUnderGround = false;
    private Vector3 _fallingVelocity = new Vector3(0, 1, 0);



    private PlayerController _playerController;
    private ObjectPooler _objectPooler;
    private AudioManager _audioManager;
    private DropsManager _dropsManager;
    private EnemyManager _enemyManager;


    // Substituir por Dificuladade do jogo
    private float _chanceToSpawn = 0.25f;

    public override void GetHit(float damage)
    {
        PlayRandomAudio(CharacterD.GetHitAudios);

        float blockModfire = 0;

        if(_isBlocking)
        {
            blockModfire = Armor.BlockResistence;
            CharacterAnimator.SetTrigger(EnemyAnimationData.BlockBeHit);
        }
        else
        {
            CharacterAnimator.SetTrigger(EnemyAnimationData.BeHit);
        }

        if (_navMashAgent.destination != null)
        {
            _navMashAgent.isStopped = true;
        }


        _canWalk = false;
        AnimatorClipInfo[] clipInfo = CharacterAnimator.GetCurrentAnimatorClipInfo(0);
        float time = clipInfo[0].clip.length;

        StartCoroutine(CanWalkCooldown(1));

        float actualDamage = damage - Armor.ArmorResistence - blockModfire;

        if(actualDamage > 0)
        {
            HealthControl.LoseHealth(damage);
        }
    }

    public override void Initialize()
    {
        base.Initialize();

        HealthControl.MaxHealth = CharacterD.MaxHealth;
        HealthControl.ResetHealth();

        _navMashAgent.speed = CharacterD.WalkSpeed;
        _navMashAgent.acceleration = CharacterD.Acceleration;

        _target = GameManager.sInstance.GetPlayerController();
        _destination = _target.transform;

        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();
        
        _dropsManager = GameManager.sInstance.GetDropsManager();
        _enemyManager = GameManager.sInstance.GetEnemyManager();
        _playerController = GameManager.sInstance.GetPlayerController();

        _animationHandle.Enemy = this;

        SetHitAreaPosition();
    }

    public virtual void Attack()
    {
        _aiming = false;

        float actualDamage = Weapon.Damage;
        
        Collider[] hitTarget = Physics.OverlapSphere(_hitCollider.position, Weapon.WeaponRange / 2);

        foreach(Collider target in hitTarget)
        {
            if(target.gameObject.GetComponent<IDamageable<float>>() != null)
            {
                target.gameObject.GetComponent<IDamageable<float>>().GetHit(actualDamage);

            }
            
            PlayRandomAudio(Weapon.HitAudios);
        }
        
        if(hitTarget.Length == 0)
        {
            PlayRandomAudio(Weapon.MissAudios);
        }

    }
    public virtual void FootR()
    {
        //Debug.Log("FootR");
    }
    public virtual void FootL()
    {
        //Debug.Log("FootL");
    }




    protected override void Die()
    {
        StopAllCoroutines();

        PlayRandomAudio(CharacterD.DyingAudios);

        CharacterCollider.enabled = false;
        _navMashAgent.isStopped = true;
        _navMashAgent.enabled = false;

        _enemyManager.KillCount++;
        _enemyManager.LessOneEnemieInGame(CharacterD.killScoreValue);

        TrySpawnDrop();

        StartCoroutine(Dying());
    }

    protected virtual void SetHitAreaPosition()
    {
        _frowardOffset = (Weapon.WeaponRange / 2) + CharacterCollider.bounds.extents.z + 0.1f;

        _hitCollider.localPosition = new Vector3(0, _hitCollider.localPosition.y, _frowardOffset);

        _navMashAgent.stoppingDistance = _frowardOffset;
    }

    protected IEnumerator TryAttackCooldown()
    {
        yield return new WaitForSeconds(Weapon.AttackCooldown);

        if(HealthControl.IsAlive)
        {
            _canAttack = true;
            _canWalk = true;
        }
    }

    protected override void MovimentHandle()
    {
        float velocityValue = 0f;

        if (_canWalk)
        {
            float distanceToDestination = Vector3.Distance(transform.position, _destination.position);

            _navMashAgent.SetDestination(_destination.position);

            velocityValue = _navMashAgent.velocity.magnitude / CharacterD.RunSpeed;

        }

        
        CharacterAnimator.SetFloat(EnemyAnimationData.Velocity, velocityValue);
    }

    protected virtual void HandleAttackControl()
    {
        if(_canAttack)
        {

            Vector3 targetXZ = new Vector3(_target.transform.position.x, 0, _target.transform.position.z);
            Vector3 selfrXZ = new Vector3(transform.position.x, 0, transform.position.z);

            float targetDistance = Vector3.Distance(targetXZ,selfrXZ);

            if(targetDistance <= _frowardOffset)
            {
                _aiming = true;
                //_destination = transform;
                CharacterAnimator.SetTrigger(EnemyAnimationData.Attack);
                StartCoroutine(TryAttackCooldown());

                _canAttack = false;
                _canWalk = false;

            }
        }

        if(_aiming)
        {
            Vector3 lookPosition = _target.transform.position;
            lookPosition = new Vector3(lookPosition.x, transform.position.y, lookPosition.z);

            transform.LookAt(lookPosition, Vector3.up);
        }
    }


    protected void PlayRandomAudio(List<Sound.SongNames> listOfSongs)
    {
        int chosenIndex = Random.Range(0, listOfSongs.Count);

        AudioSource audio = _audioManager.GetAudioSource(listOfSongs[chosenIndex]);
        audio.gameObject.transform.position = gameObject.transform.position;
    }

    private void Update()
    {
        if(_backToLife && HealthControl.IsAlive)
        {
            MovimentHandle();
            HandleAttackControl();
        }

        if(_goingUnderGround)
        {
            transform.position -= _fallingVelocity * Time.deltaTime;
        }
    }
    
    private void TrySpawnDrop()
    {
        float number = Random.Range(0f, 1f);

        if(number >= _chanceToSpawn)
        {
            int type = Random.Range(0, _dropsManager.DropsCount);

            DropItem drop = _dropsManager.GetDrop((DropInfo.DropType)type);
            drop.transform.position = transform.position;
            drop.transform.position += new Vector3(0, transform.localScale.y, 0);
            
        }

    }

    private void CanWalkAgain()
    {
        if (HealthControl.IsAlive)
        {
            _canWalk = true;
            _navMashAgent.isStopped = false;
        }
    }

    private IEnumerator CanWalkCooldown(float time)
    {
        yield return new WaitForSeconds(time);

        CanWalkAgain();
    }

    private IEnumerator Dying()
    {
        CharacterAnimator.SetTrigger(EnemyAnimationData.Die);
        
        yield return new WaitForSeconds(5f);
        _goingUnderGround = true;
        yield return new WaitForSeconds(5f);

        _goingUnderGround = false;
        gameObject.SetActive(false);
    }

    private IEnumerator ComeBackToLife()
    {
        yield return new WaitForSeconds(2.5f);

        _navMashAgent.enabled = true;
        _navMashAgent.isStopped = false;
        CharacterCollider.enabled = true;
        _backToLife = true;

        CanWalkAgain();
    }


    private void OnEnable()
    {
        if(_enemyManager == null)
        {
            _enemyManager = GameManager.sInstance.GetEnemyManager();
        }

        _canAttack = true;
        HealthControl.ResetHealth();

        CharacterAnimator.transform.localPosition = Vector3.zero;
        _enemyManager.NewEnemieInGame();

        StartCoroutine(ComeBackToLife());
    }

    private void OnDisable()
    {
        _navMashAgent.enabled = false;
        _objectPooler.ReturnGameObject(gameObject);
    }


    protected bool CanWalk
    {
        get { return _canWalk; }
        set { _canWalk = value; }
    }

    protected bool CanAttack
    {
        get { return _canAttack; }
        set { _canAttack = value; }
    }

    protected ObjectPooler ObjPooler
    {
        get { return _objectPooler; }
    }

    protected Transform Destination
    {
        get { return _destination; }
        set { _destination = value; }
    }

    protected PlayerController Player
    {
        get { return _playerController; }
    }

    protected NavMeshAgent EnemyNavMeshAgent
    {
        get { return _navMashAgent; }
        set { _navMashAgent = value; }
    }

}
