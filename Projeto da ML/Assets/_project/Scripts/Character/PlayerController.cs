﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
    [SerializeField] private Gun _bow = default;

    [Header("Components")]
    [SerializeField] private MouseLook _mouseLook = default;
    [SerializeField] private Movement _movement = default;
    [SerializeField] private ParticleSystem _bloodEffect = default;

    [SerializeField] private CameraShake _cameraShake = default;
    [SerializeField] private float _shakeIntencity = 4;
    [SerializeField] private float _GetHitDuration = 0.5f;

    private InputListner _inputListener;
    private AudioManager _audioManager;
    private GameController _gameController;
    private DistorceView _distorceView = default;

    private float _strangth = 10;

    public override void Initialize()
    {
        base.Initialize();

        _distorceView = GameManager.sInstance.GetDistorceView();
        _inputListener = GameManager.sInstance.GetInputListner();
        _gameController = GameManager.sInstance.GetGameController();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        _movement.Setup(CharacterD);

        _inputListener.OnPlayerInput += ReceiveInputs;
        _inputListener.OnPlayerInput += _movement.ReceiveInputs;
        _inputListener.OnPlayerInput += _mouseLook.ReceiveInputs;

        _distorceView.Initialize();
    }

    public override void GetHit(float damage)
    {
        PlayRandomAudio(CharacterD.GetHitAudios);
        HealthControl.LoseHealth(damage);
        _cameraShake.ShakeCamera(_shakeIntencity, _GetHitDuration);
        _distorceView.ChangeSaturation(_GetHitDuration);
        _bloodEffect.Play();
    }

    protected override void Die()
    {
        _gameController.LoseGame();
    }

    protected override void MovimentHandle()
    {
    }

    protected void PlayRandomAudio(List<Sound.SongNames> listOfSongs)
    {
        int chosenIndex = Random.Range(0, listOfSongs.Count);

        AudioSource audio = _audioManager.GetAudioSource(listOfSongs[chosenIndex]);
        audio.gameObject.transform.position = gameObject.transform.position;
    }

    private void ReceiveInputs(InputData inputData)
    {
        if (Time.timeScale != 0f)
        {
            if (inputData.Shoot)
            {
                _bow.IncreaseStrangth(_strangth * Time.deltaTime);
            }
            else
            {
                _bow.Shoot();
            }
        }
    }


    public Gun CharacterGun
    {
        get { return _bow; }
        set { _bow = value; }
    }

}
