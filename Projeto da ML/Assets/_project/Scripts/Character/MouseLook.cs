﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private Vector2 _mouseSensivity = new Vector2();
    [SerializeField] private Vector2 _mouseValue = new Vector2();

    [SerializeField] private Transform _targetTrasnform = default;
    [SerializeField] private float _clamp = 85f;
    
    private float _rotationX = 0f;


    public void ReceiveInputs(InputData inputData)
    {
        _mouseValue.x = inputData.MouseMovement.x * Time.deltaTime * _mouseSensivity.x;
        _mouseValue.y = inputData.MouseMovement.y * Time.deltaTime * _mouseSensivity.y;
    }


    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        HandleRotation();
    }

    private void HandleRotation()
    {
        transform.Rotate(Vector3.up, _mouseValue.x);
        
        _rotationX -= _mouseValue.y;
        _rotationX = Mathf.Clamp(_rotationX, -_clamp, _clamp);

        Vector3 targetRotation = transform.eulerAngles;
        targetRotation.x = _rotationX;

        _targetTrasnform.eulerAngles = targetRotation;
    }
}
