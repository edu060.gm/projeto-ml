﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterData", menuName = "ScriptableObject/CharacterData")]
public class CharacterData : ScriptableObject
{
    
    public string Name;

    public float MaxHealth;

    public int killScoreValue;

    public float WalkSpeed;
    public float RunSpeed;

    public float Acceleration;

    public List<Sound.SongNames> DyingAudios;
    public List<Sound.SongNames> GetHitAudios;
}
