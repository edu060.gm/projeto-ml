﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private CharacterController _characterController = default;

    [Header("Movement")]
    private Vector2 _direction;
    
    private float _runModfire;
    private float _walkSpeed;
    private float _runSpeed;

    private bool _isRunning;



    [Header("Jump")]
    [SerializeField] private AnimationCurve _jumpCurve = default;
    [SerializeField] private float _jumpStrength = default;

    private bool _jumpPressed;
    private bool _isJumping;


    public void ReceiveInputs(InputData inputData)
    {
        _direction = inputData.Movement;
        _isRunning = inputData.Run;
        _jumpPressed = inputData.Jump;
    }

    public void Setup(CharacterData characterData)
    {
        _walkSpeed = characterData.WalkSpeed;
        _runSpeed = characterData.RunSpeed;
    }


    private void Update()
    {
        DoMovement(_direction);
    }

    private void DoMovement(Vector2 direcion)
    {

        if (_isRunning)
        {
            _runModfire = _runSpeed;
        }
        else
        {
            _runModfire = 1;
        }
        
        Vector3 horizontalMoviment = (transform.right * direcion.x + transform.forward * direcion.y);

        _characterController.SimpleMove(Vector3.ClampMagnitude(horizontalMoviment, 1) * _walkSpeed * _runModfire);

        Jump();
    }

    private void Jump()
    {
        if(_jumpPressed && _isJumping == false)
        {
            _isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    private IEnumerator JumpEvent()
    {
        float timeInAir = 0f;


        do
        {
            float jumpForce = _jumpCurve.Evaluate(timeInAir);

            _characterController.Move(Vector3.up * _jumpStrength * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;
        } while (!_characterController.isGrounded && _characterController.collisionFlags != CollisionFlags.Below);

        _isJumping = false;
    }


    public float MaxSpeed
    {
        get { return _walkSpeed; }
        set { _walkSpeed = value; }
    }
}
