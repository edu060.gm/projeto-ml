﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour, IDamageable<float>
{
    [Header("DataFiles")]
    [SerializeField] private CharacterData _characterData = default;
    [SerializeField] private WeaponData _weaponData = default;
    [SerializeField] private ArmorData _armorData = default;

    [Header("BaseComponents")]
    [SerializeField] private HealthController _healthController = default;
    [SerializeField] private Collider _collider = default;
    [SerializeField] private Animator _animator = default;

    public abstract void GetHit(float damage);
    protected abstract void MovimentHandle();
    protected abstract void Die();


    public virtual void Initialize()
    {
        _healthController.MaxHealth = _characterData.MaxHealth;
        _healthController.ResetHealth();
        _healthController.OnDie += Die;
    }

    private void Start()
    {
        Initialize();
    }


    public CharacterData CharacterD
    {
        get { return _characterData; }
    }
    public WeaponData Weapon
    {
        get { return _weaponData; }
        set { _weaponData = value; }
    }
    public ArmorData Armor
    {
        get { return _armorData; }
        set { _armorData = value; }
    }




    public Animator CharacterAnimator
    {
        get { return _animator; }
        set { _animator = value; }
    }

    public HealthController HealthControl
    {
        get { return _healthController; }
        set { _healthController = value; }
    }

    public Collider CharacterCollider
    {
        get { return _collider; }
        set { _collider = value; }
    }
}
