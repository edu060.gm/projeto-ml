﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : BaseEnemy
{
    [SerializeField] private Gun _gun = default;
    [SerializeField] private float _strangth;

    private bool _aimin = true;


    public override void Initialize()
    {
        base.Initialize();

        //HealthControl.OnChangeHealth += CheckRunAway;
    }

    public override void Attack()
    {
        _aimin = false;
        
        _gun.IncreaseStrangth(_strangth);
        _gun.Shoot();
        _gun.Reload();
        _gun.Ammo++;
    }

    protected override void SetHitAreaPosition()
    {
    }

    protected override void MovimentHandle()
    {
    }

    protected override void HandleAttackControl()
    {
        if(CanAttack)
        {
            CharacterAnimator.SetTrigger(EnemyAnimationData.Attack);
            StartCoroutine(TryAttackCooldown());
            CanAttack = false;
            CanWalk = false;
            _aimin = true;

        }

        if(_aimin)
        {
            Vector3 lookPosition = Player.transform.position;
            lookPosition = new Vector3(lookPosition.x, transform.position.y, lookPosition.z);
            
            transform.LookAt(lookPosition, Vector3.up);
        }
    }







}
