﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public delegate void GameEndDelegate(bool win);
    public GameEndDelegate OnGameEnd;

    // Timer definitions 
    [Header("Timer")]
    [SerializeField] private Text _textTimer = default;

    private float _secondsRemening = 120f;
    private float _waitToStartTime = 3;

    [SerializeField] private DropsSpawner _dropsSpawner = default;


    private bool _gameStarted = false;

     
    private EnemySpawnManager _enemySpawnManager;
    private AudioManager _audioManager;
    private EnemyManager _enemyManager;

    private AudioSource _combatTheme;


    public void Initialize()
    {
        _audioManager = PersistemObjects.sInstance.GetAudioManager();
        _enemyManager = GameManager.sInstance.GetEnemyManager();

        _combatTheme = _audioManager.GetAudioSource(Sound.SongNames.COMBAT);
        _combatTheme.Pause();

        _secondsRemening = MathSystem.CalculateTimeByModeSelected(SaveSystem.LocalData.GameTime);

        UpdateClockText(_waitToStartTime);

        StartCoroutine(SecondFeedBack());
    }

    public void LoseGame()
    {
        EndGame();
        OnGameEnd?.Invoke(false);
    }

    private void WinGame()
    {
        EndGame();
        OnGameEnd?.Invoke(true);
    }

    private IEnumerator SecondFeedBack()
    {
        yield return new WaitForSeconds(1f);

        _audioManager.GetAudioSource(Sound.SongNames.SECOND);
        _waitToStartTime -= 1f;

        if (_waitToStartTime > 0)
        {
            StartCoroutine(SecondFeedBack());
        }
        else
        {
            StartTheGame();
        }

        UpdateClockText(_waitToStartTime);
    }

    private void UpdateClockText(float time)
    {
        int minutes = (int)(time / 60);
        int seconds = (int)(time % 60);

        _textTimer.text = minutes + ":";

        if(seconds <= 9)
        {
            _textTimer.text += "0";
        }

        _textTimer.text += seconds;
    }

    private void Update()
    {
        if(_gameStarted)
        {
            UpdateClockText(_secondsRemening);
            _secondsRemening -= Time.deltaTime;

            if(_secondsRemening < 0f)
            {
                _gameStarted = false;

                WinGame();
            }
        }
    }


    private void BackToMenu(ConfirmationMenu confirmation)
    {
        PlayButtonAudio();
        confirmation.gameObject.SetActive(true);
    }

    private void Exit()
    {
        PlayButtonAudio();
        Time.timeScale = 1f;
        Loader.Load(Loader.Scene.MainMenu);

    }

    private void EndGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        _combatTheme.Pause();

        Time.timeScale = 0f;
    }

    private void StartTheGame()
    {
        _combatTheme.Play();
        _gameStarted = true;
        _dropsSpawner.Initialize();
    }

    private void PlayButtonAudio()
    {
        _audioManager.GetAudioSource(Sound.SongNames.BUTTON);
    }

    public AudioSource CombatTheme
    {
        get { return _combatTheme; }
    }

    public bool GameStarted
    { 
        get { return _gameStarted; }
    }
}
