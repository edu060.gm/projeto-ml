﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneLoader : MonoBehaviour
{
    [SerializeField] private GameObject _loadingScreen = default;
    [SerializeField] private Slider _progressSlider = default;


    private ObjectPooler _objectPooler = default;

    public void Intialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadSceneAsynchrounosly(sceneIndex));
    }

    private IEnumerator LoadSceneAsynchrounosly(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        _loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            _progressSlider.value = progress;
            yield return null;
        }

        _objectPooler.ResetObjectPooler();
    }

    
}
