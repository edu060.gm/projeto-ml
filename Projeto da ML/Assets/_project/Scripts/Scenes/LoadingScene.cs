﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadingScene : MonoBehaviour
{

    [SerializeField] private Slider _loadingBar = default;

    private ObjectPooler _objectPooler;

    private void Start()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
        _objectPooler.ResetObjectPooler();
    }

    private void Update()
    {
        _loadingBar.value = Loader.GetLoadingProgress();
    }
}
