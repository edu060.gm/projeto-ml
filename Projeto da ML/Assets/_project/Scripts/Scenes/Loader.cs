﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public static class Loader
{
    private class LoadingMonoBehaviour : MonoBehaviour { }

    private static Action _onLoadCallback;
    private static AsyncOperation _asyncOperation;

    public enum Scene
    {
        MainMenu, GameMenu, Loading
    }
   
    public static void Load(Scene scene)
    {
        _onLoadCallback = () =>
        {
            GameObject loadingGameObject = new GameObject("LoadingObj");
            loadingGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadSceneAsynchrounosly(scene));
        };

        Time.timeScale = 1f;
        SceneManager.LoadScene(Scene.Loading.ToString());
    }

    public static void LoadCallback()
    {
        if(_onLoadCallback != null)
        {
            _onLoadCallback();
            _onLoadCallback = null;
        }
    }

    public static float GetLoadingProgress()
    {
        if(_asyncOperation != null)
        {
            return Mathf.Clamp01(_asyncOperation.progress / 0.9f);
        }
        else
        {
            return 1f;
        }
    }

    private static IEnumerator LoadSceneAsynchrounosly(Scene scene)
    {
        yield return null;


        _asyncOperation = SceneManager.LoadSceneAsync(scene.ToString());
    
        while(!_asyncOperation.isDone)
        {
            yield return null;
        }
    }
}
