﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCallback : MonoBehaviour
{
    private bool _isFristUpdate = true;

    private void Update()
    {
         if(_isFristUpdate)
         {
            _isFristUpdate = false;
            Loader.LoadCallback();
         }
    }
}
