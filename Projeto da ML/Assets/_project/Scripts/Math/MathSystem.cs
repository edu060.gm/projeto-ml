﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathSystem
{
    private static float _timeModfire = 30f;
    private static float _baseTime = 30f;

    public static float CalculateTimeByModeSelected(float value)
    {
        return value * _timeModfire + _baseTime;
    }



}
