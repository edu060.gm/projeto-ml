﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _startButton = default;
    [SerializeField] private Button _optionsButton = default;
    [SerializeField] private Button _exitButton = default;

    [SerializeField] private ConfirmationMenu _confirmationMenu = default;

    private Animator _cameraAnimator;
    private AudioManager _audioManager;

    private AudioSource _mainTheme;

    public void Initialize()
    {
        _cameraAnimator = MenuManager.sInstance.GetCameraAnimator();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        _mainTheme = _audioManager.GetAudioSource(Sound.SongNames.THEME);

        _startButton.onClick.AddListener(StartGame);
        _optionsButton.onClick.AddListener(GoToOptions);
        _exitButton.onClick.AddListener(ExitGame);

        _confirmationMenu.ConfirmButton.onClick.AddListener(ConfirmExit);
        _confirmationMenu.CancelButton.onClick.AddListener(CancelExit);
    }


    private void StartGame()
    {
        _cameraAnimator.SetTrigger(MeunCameraMovementData.GoToGame);
        PlayButtonAudio();
    }

    private void GoToOptions()
    {
        _cameraAnimator.SetTrigger(MeunCameraMovementData.GoToOptions);
        PlayButtonAudio();
    }

    private void PlayButtonAudio()
    {
        _audioManager.GetAudioSource(Sound.SongNames.BUTTON);
    }

    private void ExitGame()
    {
        PlayButtonAudio();
        _confirmationMenu.gameObject.SetActive(true);
    }

    private void ConfirmExit()
    {
        PlayButtonAudio();
        Application.Quit();
    }
    private void CancelExit()
    {
        PlayButtonAudio();
        _confirmationMenu.gameObject.SetActive(false);
    }

    public AudioSource MainTheme
    {
        get { return _mainTheme; }
        set { _mainTheme = value; }
    }

}
