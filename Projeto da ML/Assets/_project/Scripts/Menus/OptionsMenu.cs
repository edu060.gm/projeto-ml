﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private Button _backButton;

    [SerializeField] private Slider _masterVolume = default;
    [SerializeField] private Slider _effectVolume = default;
    [SerializeField] private Slider _musicsVolume = default;

    [SerializeField] private Slider _gameTime = default;

    [SerializeField] private AudioMixer _mixerMaster = default;

    [SerializeField] private Text _timeGameText = default;

    private Animator _cameraAnimator;
    private AudioManager _audioManager;

    private AudioSource _optionsTheme;

    public void Initialize()
    {
        _cameraAnimator = MenuManager.sInstance.GetCameraAnimator();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        _optionsTheme = _audioManager.GetAudioSource(Sound.SongNames.OPTIONS);
        _optionsTheme.Pause();

        LoadSaveConfigs();


        _backButton.onClick.AddListener(GoBack);
        _masterVolume?.onValueChanged.AddListener(delegate { ChangeVolume(_masterVolume.value, VolumeNameData.MasterVolume); });
        _effectVolume?.onValueChanged.AddListener(delegate { ChangeVolume(_effectVolume.value, VolumeNameData.EffectVolume); });
        _musicsVolume?.onValueChanged.AddListener(delegate { ChangeVolume(_musicsVolume.value, VolumeNameData.MusicsVolume); });

        _gameTime?.onValueChanged.AddListener(delegate { ChangeGameTime(_gameTime.value); });
    }

    private void GoBack()
    {
        _audioManager.GetAudioSource(Sound.SongNames.BUTTON);
        _cameraAnimator.SetTrigger(MeunCameraMovementData.GoToMainMenu);
    }

    private void ChangeVolume(float volumeValue, string volumeName)
    {
        float volume = Mathf.Log10(volumeValue) * 20;
        _mixerMaster.SetFloat(volumeName, volume);
        SaveVolumeChanges();
    }

    private void ChangeGameTime(float value)
    {
        _timeGameText.text = "( " + MathSystem.CalculateTimeByModeSelected(value) + "s )";

        SaveGameTime();
    }

    private void SaveVolumeChanges()
    {
        SaveSystem.LocalData.EffectVolume = _effectVolume.value;
        SaveSystem.LocalData.MasterVolume = _masterVolume.value;
        SaveSystem.LocalData.MusicsVolume = _musicsVolume.value;

        SaveSystem.SaveGame();
    }

    private void SaveGameTime()
    {
        SaveSystem.LocalData.GameTime = (int)_gameTime.value;
        SaveSystem.SaveGame();
    }

    private void LoadSaveConfigs()
    {
        SaveSystem.LoadGame();

        _effectVolume.value = SaveSystem.LocalData.EffectVolume;
        _masterVolume.value = SaveSystem.LocalData.MasterVolume;
        _musicsVolume.value = SaveSystem.LocalData.MusicsVolume;

        ChangeVolume(SaveSystem.LocalData.MasterVolume, VolumeNameData.MasterVolume);
        ChangeVolume(SaveSystem.LocalData.EffectVolume, VolumeNameData.EffectVolume);
        ChangeVolume(SaveSystem.LocalData.MusicsVolume, VolumeNameData.MusicsVolume);

        _gameTime.value = (float)SaveSystem.LocalData.GameTime;

        ChangeGameTime(SaveSystem.LocalData.GameTime);

    }

    public Slider MasterVolume
    {
        get { return _masterVolume; }
    }
    public Slider MusicsVolume
    {
        get { return _musicsVolume; }
    }
    public Slider EffectVolume
    {
        get { return _effectVolume; }
    }
    public AudioSource OptionsTheme
    {
        get { return _optionsTheme; }
        set { _optionsTheme = value; }
    }
}

