﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseHandler : MonoBehaviour
{ 
    public delegate void PauseDelegate();
    public PauseDelegate OnPauseStateChange;


    [SerializeField] private GameObject _pauseMenu = default;
    [SerializeField] private Button _continueButton = default;
    [SerializeField] private Button _backToMenuButton = default;

    [SerializeField] private ConfirmationMenu _confirmationMenu = default;

    private bool _isPaused = false;

    private InputListner _inputListner;

    public void Initialize()
    {
        _inputListner = GameManager.sInstance.GetInputListner();
        _inputListner.OnPlayerInput += ReceiveInputs;

        _continueButton.onClick.AddListener(ChangePauseState);
        _backToMenuButton.onClick.AddListener(Exit);

        _confirmationMenu.ConfirmButton.onClick.AddListener(ConfirmExit);
        _confirmationMenu.CancelButton.onClick.AddListener(CancelExit);

        _isPaused = false;
    }

    public void ChangePauseState()
    {
        _isPaused = !_isPaused;

        OnPauseStateChange?.Invoke();

        if (_isPaused)
        {
            PauseGame();
        }
        else
        {
            UnPauseGame();
        }
    }

    private void ReceiveInputs(InputData inputData)
    {
        if (inputData.MenuButton)
        {
            ChangePauseState();
        }
    }

    private void PauseGame()
    {
        _pauseMenu.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        Time.timeScale = 0f;
    }

    private void UnPauseGame()
    {
        _pauseMenu.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Time.timeScale = 1f;
    }


    private void Exit()
    {
        _confirmationMenu.gameObject.SetActive(true); ;
    }

    private void ConfirmExit()
    {
        Time.timeScale = 1f;

        Loader.Load(Loader.Scene.MainMenu);
    }
    private void CancelExit()
    {
        _confirmationMenu.gameObject.SetActive(false);
    }


    public bool IsPaused
    {
        get { return _isPaused; }
    }

}
