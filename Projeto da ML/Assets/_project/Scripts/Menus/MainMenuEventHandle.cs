﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuEventHandle : MonoBehaviour
{
    private OptionsMenu _optionsMenu;
    private MainMenu _mainMenu;


    public void StartGame()
    {
        Loader.Load(Loader.Scene.GameMenu);
    }

    public void InMainMenu()
    {
        _optionsMenu.OptionsTheme.Pause();
        _mainMenu.MainTheme.UnPause();
    }

    public void InOptionsMenu()
    {
        _mainMenu.MainTheme.Pause();
        _optionsMenu.OptionsTheme.UnPause();
    }

    public void Initialize()
    {
        _optionsMenu = MenuManager.sInstance.GetOptionsMenu();
        _mainMenu = MenuManager.sInstance.GetMainMenu();
    }

}
