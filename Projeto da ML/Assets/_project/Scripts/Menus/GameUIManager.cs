﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameUIManager : MonoBehaviour
{
    [SerializeField] private Text _arrowCounter = default;
    [SerializeField] private Image _healthBar = default;

    // PlayerInfo
    [SerializeField] private HealthController _playerHealth;
    [SerializeField] private Gun _playerGun;

    // EndGame
    [SerializeField] private EndGameMenu _winGameMenu = default;
    [SerializeField] private EndGameMenu _loseGameMenu = default;


    private GameController _gameController;
    private EnemyManager _enemyManager;
    private AudioManager _audioManager;
    private PauseHandler _pause;

    public void Initialize()
    {
        _audioManager = PersistemObjects.sInstance.GetAudioManager();
        _gameController = GameManager.sInstance.GetGameController();
        _enemyManager = GameManager.sInstance.GetEnemyManager();
        _pause = GameManager.sInstance.GetPauseHandler();

        _gameController.OnGameEnd += GameEnd;
        _playerGun.OnChangeAmmo += UpdatePlayerAmmoCount;
        _playerHealth.OnChangeHealth += UpdatePlayerHealthBar;
        UpdatePlayerHealthBar();

        _pause.OnPauseStateChange += HandleSongs;

        _loseGameMenu.Initialize();
        _winGameMenu.Initialize();

        _loseGameMenu.BackButton.onClick.AddListener(() => BackToMenu(_loseGameMenu.ConfirmMenu));
        _winGameMenu.BackButton.onClick.AddListener(() => BackToMenu(_winGameMenu.ConfirmMenu));

        _loseGameMenu.ContinueButton.onClick.AddListener(ReloadScene);
        _winGameMenu.ContinueButton.onClick.AddListener(ReloadScene);
    }

    private void GameEnd(bool winGame)
    {
        if(winGame)
        {
            _winGameMenu.gameObject.SetActive(true);
            _winGameMenu.KillCountText.text = _enemyManager.KillCount.ToString() + "!!";
            _winGameMenu.KillScoreText.text = _enemyManager.KillScore.ToString() + "!!";
        }
        else
        {
            _loseGameMenu.gameObject.SetActive(true);
            _loseGameMenu.KillCountText.text = _enemyManager.KillCount.ToString() + "!!";
            _loseGameMenu.KillScoreText.text = _enemyManager.KillScore.ToString() + "!!";
        }
    }

    private void ReloadScene()
    {
        Loader.Load(Loader.Scene.GameMenu);
    }

    private void BackToMenu(ConfirmationMenu confirmation)
    {
        confirmation.gameObject.SetActive(true);
    }

    private void Exit()
    {
        Loader.Load(Loader.Scene.MainMenu);
        Time.timeScale = 1f;
    }

    private void UpdatePlayerHealthBar()
    {
        _healthBar.fillAmount = _playerHealth.HealthPerCent;
    }

    private void UpdatePlayerAmmoCount()
    {
        _arrowCounter.text = _playerGun.Ammo.ToString() + " / " + _playerGun.MaxAmmo;
    }

    private void HandleSongs()
    {
        if(_pause.IsPaused)
        {
            _gameController.CombatTheme.Pause();
        }
        else
        {
            _gameController.CombatTheme.UnPause();
        }
    }
}
