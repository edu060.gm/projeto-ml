﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public static MenuManager sInstance;

    [SerializeField] private Animator _cameraAnimator = default;
    [SerializeField] private MainMenuEventHandle _menuEvents = default; 
    [SerializeField] private OptionsMenu _optionsMenu = default;
    [SerializeField] private MainMenu _mainMenu = default;

    private void Awake()
    {
        if(sInstance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            sInstance = this;
        }
    }

    public Animator GetCameraAnimator()
    {
        return _cameraAnimator;
    }

    public OptionsMenu GetOptionsMenu()
    {
        return _optionsMenu;
    }

    public MainMenu GetMainMenu()
    {
        return _mainMenu;
    }


    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _optionsMenu?.Initialize();
        _mainMenu?.Initialize();
        _menuEvents?.Initialize();
    }


}
