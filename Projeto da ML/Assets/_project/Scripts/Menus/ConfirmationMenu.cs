﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ConfirmationMenu : MonoBehaviour
{
    [SerializeField] private Button _confirmButton = default;
    [SerializeField] private Button _cancelButton = default;

    public void Initialize()
    {
        _cancelButton.onClick.AddListener(Cancel);
    }

    private void Cancel()
    {
        gameObject.SetActive(false);
    }

    public Button ConfirmButton
    {
        get { return _confirmButton; }
    }

    public Button CancelButton
    {
        get { return _cancelButton; }
    }



}
