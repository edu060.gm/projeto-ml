﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameMenu : MonoBehaviour
{

    [SerializeField] private Button _backButton = default;
    [SerializeField] private Button _continueButton = default;

    [SerializeField] private Text _killCountText = default; 
    [SerializeField] private Text _killScoreText = default;

    [SerializeField] private ConfirmationMenu _confirmationMenu = default;

    [SerializeField] private Sound.SongNames _menuThemeName = default;

    private AudioManager _audioManager;
    private AudioSource _menuTheme;

    public void Initialize()
    {
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        _menuTheme = _audioManager.GetAudioSource(_menuThemeName);
        _menuTheme.Stop();
    }

    private void OnEnable()
    {
        _menuTheme.Play();
    }

    public Text KillCountText
    {
        get { return _killCountText; }
    }
    public Text KillScoreText
    {
        get { return _killScoreText; }
    }

    public Button BackButton
    {
        get { return _backButton; }
    }
    public Button ContinueButton
    {
        get { return _continueButton; }
    }

    public ConfirmationMenu ConfirmMenu
    {
        get { return _confirmationMenu; }
    }
}
