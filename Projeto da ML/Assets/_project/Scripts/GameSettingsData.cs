﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Settings", menuName = "ScriptableObject/GameSettingsData")]
public class GameSettingsData : ScriptableObject
{

    public float GameTime;
    public int MaxEnemies;



}
