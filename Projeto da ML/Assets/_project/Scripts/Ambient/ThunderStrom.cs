﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderStrom : MonoBehaviour
{
    [SerializeField] private int _thunderSamplesAmount = default;

    [SerializeField] private float _minThunderGap = 3;
    [SerializeField] private float _maxThunderGap = 10;

    [SerializeField] private Color32 _baseWorldLigthColor = default;
    [SerializeField] private Color32 _baseThunderColor = default;

    [SerializeField] private Light _directionalLight = default;

    private AudioManager _audioManager;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        SaveLightConfig();

        float timeGap = Random.Range(_minThunderGap, _maxThunderGap);
        StartCoroutine(SpawnThunder(timeGap));
    }


    private void SaveLightConfig()
    {
        _baseWorldLigthColor = _directionalLight.color;
    }

    private IEnumerator ThunderFlash()
    {
        _directionalLight.color = _baseThunderColor;
        yield return new WaitForSeconds(.1f);
        _directionalLight.color = _baseWorldLigthColor;
    }

    private IEnumerator SpawnThunder(float time)
    {
        yield return new WaitForSeconds(time);

        _audioManager.GetAudioSource(ChooseStorm());
        StartCoroutine(ThunderFlash());
        float timeGap = Random.Range(_minThunderGap, _maxThunderGap);
        StartCoroutine(SpawnThunder(timeGap));
    }

    private Sound.SongNames ChooseStorm()
    {
        int choose = Random.Range(0, _thunderSamplesAmount);

        switch(choose)
        {
            case 0:
                return Sound.SongNames.THUNDER_1;
            case 1:
                return Sound.SongNames.THUNDER_2;
            case 2:
                return Sound.SongNames.THUNDER_3;
        }

        return Sound.SongNames.THUNDER_1;
    }

}
