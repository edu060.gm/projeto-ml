﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class DistorceView : MonoBehaviour
{
    [SerializeField] private Volume _postProcessig = default;

    private ColorAdjustments _colorAdjustments = default;
    private Bloom _bloom = default;
    private float _saturationValue = default;

    public void Initialize()
    {
        _postProcessig.profile.TryGet(out _bloom);
        _postProcessig.profile.TryGet(out _colorAdjustments);

        //_bloom = _postProcessig.GetComponent<Bloom>();
        //_colorAdjustments = _postProcessig.GetComponent<ColorAdjustments>();

        _saturationValue = _colorAdjustments.saturation.value;
    }

    public void ChangeSaturation(float duration)
    {
        _colorAdjustments.saturation.value = -56f;

        StartCoroutine(ResetSaturation(duration));
    }

    private IEnumerator ResetSaturation(float time)
    {
        yield return new WaitForSeconds(time);
        _colorAdjustments.saturation.value = _saturationValue;
    }
}
