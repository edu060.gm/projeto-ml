﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandle : MonoBehaviour
{

    [SerializeField] private BaseEnemy _characterReference = default;


    public void FootR()
    {
        _characterReference.FootR();
    }

    public void FootL()
    {
        _characterReference.FootL();
    }

    public void Hit()
    {
        _characterReference.Attack();
    }


    public BaseEnemy Enemy
    {
        get { return _characterReference; }
        set { _characterReference = value; }
    }
}
