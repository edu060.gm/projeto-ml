﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    [SerializeField] private float _health;
    [SerializeField] private float _maxHealth;

    private bool _imortal = false;
    private bool _isAlive = true;

    private float _healthPerCent = 1f;

    public delegate void HealthHandleDelegate();
    public HealthHandleDelegate OnDie;
    public HealthHandleDelegate OnChangeHealth;

    public bool CheckFullLife()
    {
        if(_health >= _maxHealth)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ResetHealth()
    {
        _health = _maxHealth;
        _isAlive = true;
    }

    public void AddHealth(float bonusHealth)
    {
        _health += bonusHealth;

        if (_health >= _maxHealth)
        {
            _health = _maxHealth;
        }

        UpdateHealthPerCent();
        OnChangeHealth?.Invoke();
    }

    public void LoseHealth(float damageTaken)
    {
        if (_isAlive && _imortal == false)
        {
            _health -= damageTaken;
            HandleDeath();

            UpdateHealthPerCent();
            OnChangeHealth?.Invoke();
        }
    }

    private void HandleDeath()
    {
        if (_health <= 0)
        {
            OnDie?.Invoke();
            _isAlive = false;
        }
        else
        {
            _isAlive = true;
        }

    }

    private void UpdateHealthPerCent()
    {
        _healthPerCent = Health * 1 / _maxHealth;
    }


    public float Health
    {
        get { return _health; }
        set { _health = value; }
    }
    public bool IsAlive
    {
        get { return _isAlive; }
        set { _isAlive = value; }
    }

    public bool Imortal
    {
        get { return _imortal; }
        set { _imortal = value; }
    }

    public float MaxHealth
    {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }

    public float HealthPerCent
    {
        get { return _healthPerCent; }
    }
}
