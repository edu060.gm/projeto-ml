﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera = default;

    private CinemachineBasicMultiChannelPerlin _cinemachineBasicMultiChannelPerlin;

    public void ShakeCamera(float intensity = 4, float time = 0.5f)
    {
        _cinemachineBasicMultiChannelPerlin.m_FrequencyGain = intensity;
        StartCoroutine(WaitTime(time));
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _cinemachineBasicMultiChannelPerlin = _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private IEnumerator WaitTime(float time)
    {
        yield return new WaitForSeconds(time);
        _cinemachineBasicMultiChannelPerlin.m_FrequencyGain = 0f;
    }

}
