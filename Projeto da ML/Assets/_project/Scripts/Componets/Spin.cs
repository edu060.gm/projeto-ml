﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] private Vector3 _velocity = new Vector3(0,100f,0);

    // Update is called once per frame
    void Update()
    {
        Spining();
    }

    private void Spining()
    {
        transform.Rotate(_velocity * Time.deltaTime);
    }

}
