﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armor", menuName = "ScriptableObject/ArmorData")]
public class ArmorData : ScriptableObject
{
    public string Name;
    public float ArmorResistence;
    public float BlockResistence;

}
