﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveDraw : MonoBehaviour
{


    [Header("Base Definitions")]
    //[SerializeField] private LayerMask _collidableLayers = default;
    [SerializeField] private LineRenderer _lineRenderer = default;
    [SerializeField] private bool _showing = false;
    [SerializeField] private Gun _gun = default;

    private Vector3 _startPosition;
    private Vector3 _startVelocity;

    private float _timeBetweenPoints = 0.1f;
    private int _numPoints = 20;


    public void Setup(Gun gun)
    {
        _gun = gun;
    }

    private void Start()
    {
        _startPosition = transform.position;
        _startVelocity = new Vector3(1, 1, 0);
    }


    private void Update()
    {
        if(_showing)
        {
            if (_lineRenderer.enabled == false)
            {
                _lineRenderer.enabled = true;
            }

            UpdateLine();
        }
        else if(_lineRenderer.enabled)
        {
            _lineRenderer.enabled = false;
        }
        
    }

    private void UpdateLine()
    {
        _lineRenderer.positionCount = _numPoints;
        List<Vector3> points = new List<Vector3>();

        _startPosition = _gun.ShotPoint.position;
        _startVelocity = _gun.ShotPoint.forward * _gun.ShotStrangth;

        for (float t = 0; t < _numPoints; t += _timeBetweenPoints)
        {
            Vector3 newPoint = _startPosition + t * _startVelocity;

            newPoint.y = _startPosition.y + _startVelocity.y * t + Physics.gravity.y / 2f * t * t;

            points.Add(newPoint);
        }

        _lineRenderer.SetPositions(points.ToArray());

    }


    public bool ShowLine
    {
        get { return _showing; }
        set { _showing = value; }
    }
}
