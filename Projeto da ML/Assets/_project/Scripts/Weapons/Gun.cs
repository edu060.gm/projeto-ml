﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private GameObject _projectilePrefab = default;
    [SerializeField] private GameObject _ammoVisualizer = default;
    [SerializeField] private Transform _shotPosition = default;
    [SerializeField] private CurveDraw _curveDraw = default;
    [SerializeField] private Animator _animator = default;

    [SerializeField] private GunData _gunData = default;


    public delegate void GunDelegate();
    public GunDelegate OnChangeAmmo;


    private float _shotStrangth = 8f;
    private float _minShotStrangth = 8;
    private float _maxShotStrangth = 30;

    private int _ammo = 8;

    private int _maxAmmoCapacity = 8;

    private ObjectPooler _objectPooler = default;
    private AudioManager _audioManager = default;
    
    private bool _isCharged = false;
    private bool _canShoot = true;


    public void HandleAmmo()
    {
        if(!CheckAmmo())
        {
            _canShoot = false;
            _ammoVisualizer?.SetActive(false);
        }
        else 
        {
            _ammoVisualizer?.SetActive(true);
        }

        OnChangeAmmo?.Invoke();
    }

    public void Reload()
    {
        if (CheckAmmo())
        {
            _canShoot = true;
        }
    }

    public void AddAmmo()
    {
        _ammo++;

        if(_ammo >= _maxAmmoCapacity)
        {
            _ammo = _maxAmmoCapacity;
        }
    }

    public bool CheckIfIsFullAmmo()
    {
        if(_ammo >= _maxAmmoCapacity)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void IncreaseStrangth(float increaseStrangth)
    {
        if (_canShoot)
        {
            _shotStrangth += increaseStrangth;

            if (_shotStrangth >= _maxShotStrangth)
            {
                _shotStrangth = _maxShotStrangth;
            }

            float strangth = (_shotStrangth - _minShotStrangth) / (_maxShotStrangth - _minShotStrangth);
            
            if(_animator)
            {
                _animator.SetFloat("Strangth", strangth);
            }

            if (!_isCharged)
            {
                _isCharged = true;

                if(_animator)
                {
                    _animator.SetTrigger("ArrowPull");
                }
                
                if(_curveDraw)
                {
                    _curveDraw.ShowLine = true;
                }
            }
        }
    }

    protected void PlayRandomAudio(List<Sound.SongNames> listOfSongs)
    {
        if(listOfSongs.Count > 0)
        {
            int chosenIndex = Random.Range(0, listOfSongs.Count);

            AudioSource audio = _audioManager.GetAudioSource(listOfSongs[chosenIndex]);
            audio.gameObject.transform.position = gameObject.transform.position;
        }
    }

    private bool CheckAmmo()
    {
        if(_ammo <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public void Shoot()
    {
        if(_isCharged)
        {
            if(_animator)
            {
                _animator.SetTrigger("ArrowRelease");
            }

            PlayRandomAudio(_gunData.ShootAudios);

            GameObject projectile = Instantiate(_projectilePrefab, _shotPosition.position, _shotPosition.rotation);
            projectile.GetComponent<Rigidbody>().velocity = _shotPosition.forward * _shotStrangth;

            _isCharged = false;
            _canShoot = false;
            _ammo--;

            if(_curveDraw)
            { 
                _curveDraw.ShowLine = false;
            }

            HandleAmmo();

            ResetStrangth();

        }
    }


    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _objectPooler = PersistemObjects.sInstance.GetObjectPooler();
        _audioManager = PersistemObjects.sInstance.GetAudioManager();

        _maxAmmoCapacity = _gunData.MaxAmmo;
    }

    private void ResetStrangth()
    {
        _shotStrangth = _minShotStrangth;
    }


    public float ShotStrangth
    {
        get { return _shotStrangth; }
        set { _shotStrangth = value; }
    }

    public Transform ShotPoint
    {
        get { return _shotPosition; }
        set { _shotPosition = value; }
    }

    public int Ammo
    {
        get { return _ammo; }
        set { _ammo = value; }
    }

    public int MaxAmmo
    {
        get { return _maxAmmoCapacity; }
    }

}
