﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "ScriptableObject/WeaponData")]
public class WeaponData : ScriptableObject
{
    public string Name;
    public float WeaponTryAttackRange;
    public float WeaponRange;
    public float Damage;
    public float AttackCooldown;

    public List<Sound.SongNames> MissAudios;
    public List<Sound.SongNames> HitAudios;
}
