﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "ScriptableObject/GunData")]
public class GunData : ScriptableObject
{
    public string Name;
    public float Damage;

    public int MaxAmmo;


    public List<Sound.SongNames> ShootAudios;
    public List<Sound.SongNames> ReloadAudios;
}
